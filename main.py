'''
    Convert aiml to json with xml2json (https://github.com/hay/xml2json)
    $ xml2json -t json2xml -o file.xml file.json
'''

import sys
import json
import re
import random
from pprint import pprint

class Processor:

    def __init__(self):
        with open('aijson/bot_profile.json') as data_file:    
            self.data = json.load(data_file)
        
        self.topic = ''
        self.template_text = 'I did not understand your query'
        self.aiml = self.data.get('aiml')
        self.category = self.aiml.get('category')
    
    def compareMatches(self,line):
        for cat in self.category:
            pattern = cat.get('pattern')
            # get the pattern
            if type(pattern) is str:
                pattern_text = pattern
            else:
                pattern_text = pattern.get('#text')
                self.topic = pattern.get('set',None)

            if re.search(pattern_text,line,re.IGNORECASE):
                template = cat.get('template')
                # pprint(template)
                if isinstance(template,dict):
                    self.template_text = template.get('#text',None)
                    if self.template_text is None:
                        random_list = template.get('random',None)
                        if random_list is not None:
                            li = random_list.get('li',None)
                            if li is not None:
                                # pprint(li)
                                list_item = random.choice(li)
                                if isinstance(list_item,dict):
                                    self.template_text = li.get('#text',None)
                                else:
                                    self.template_text = list_item
                                    

                    if isinstance(self.template_text,list):
                        self.template_text = self.template_text[0]
                else:
                    self.template_text = template
        pprint(self.template_text)
if __name__ == "__main__":
    p = Processor()
    for line in sys.stdin:
        p.compareMatches(line)
        # pprint(p.template_text)
    